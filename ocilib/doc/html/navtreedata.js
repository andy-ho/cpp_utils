var NAVTREE =
[
  [ "OCILIB (C and C++ Driver for Oracle)", "index.html", [
    [ "Introduction", "index.html", [
      [ "Description", "index.html#Intro", null ],
      [ "Version information", "index.html#Version", null ],
      [ "Main features", "index.html#Features", null ],
      [ "Download", "index.html#Download", null ],
      [ "Author", "index.html#Author", null ],
      [ "License", "index.html#License", null ],
      [ "ChangeLog", "index.html#Changelog", null ]
    ] ],
    [ "Documentation", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"classocilib_1_1_direct_path.html#aa7784b877512190c4c90274e5a599685",
"classocilib_1_1_long.html#ae36ce43f57230c6c28bc29d9a5344807",
"classocilib_1_1_timestamp.html#a02e61b3823bd2c30cfd8c88c5b020c97",
"group___ocilib_c_api_binding.html#ga6fe7cc3597c3362094ce526faa79fc8b",
"group___ocilib_c_api_fetching.html#ga2a7ec7c2c056dd145ab32bc41fabcd2d",
"group___ocilib_c_api_subscriptions.html#gaac2b2c3f7bc5e07f230cb4f797f95e8c"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';