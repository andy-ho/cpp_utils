#pragma once
#include "NewHeaderCtrl.h"
#include <map>
#include <vector>

struct ListNotifyData
{
	int nRow;
	int nCol;
	CString cstrText;
	CRect rcItem;
	CWnd* pWnd;

	ListNotifyData()
		: nRow(-1)
		, nCol(-1)
		, pWnd(NULL)
	{

	}
};

enum ColumnControlType
{
	ListColumnText,
	ListColumnEdit,
	ListColumnCheckBox, // 不要使用ListCtrl默认的LVS_EX_CHECKBOXES风格
	ListColumnComboBox,
};


//自定义颜色，指定行使用这种颜色显示
#define MAX_CUSTOM_COLOR 10
class CustomColor
{
public:
	CustomColor() {};
	CustomColor(int nColorID, int nColomn, CString strTxt, COLORREF bgColor, COLORREF fontColor)
		: m_nColorID(nColorID),
		m_nColumn(nColomn),
		m_strTxt(strTxt)
	{
		SetBgColor(bgColor);
		SetFontColor(fontColor);
	}

	~CustomColor()
	{
//		m_brushColor.DeleteObject();
	}

	int GetColorID() { return m_nColorID; }
	void SetColorID(int nColorID) { m_nColorID = nColorID; }

	CString GetColorInfo(int& nColomn) { nColomn = m_nColumn; return m_strTxt; }
	void SetColorInfo(int nColomn, CString strTxt) { m_nColumn = nColomn; m_strTxt = strTxt; }

	COLORREF GetBgColor() { return m_bgColor; }
	void SetBgColor(COLORREF color)
	{
		m_bgColor = color;
	}

	COLORREF GetFontColor() { return m_fontColor; }
	void SetFontColor(COLORREF color)
	{
		m_fontColor = color;
	}

private:
	COLORREF m_bgColor;
	COLORREF m_fontColor;
	int m_nColorID;
	int m_nColumn;
	CString m_strTxt;
};

struct SumItem //合计行，锁定操作
{
	BOOL bEnable = FALSE;
	int nColumn;
	CString strTxt;
};


struct OrderColumn  //序号列
{
	BOOL bEnable = FALSE;
	int nColumn;
	CString strTxt;
};

class CBuddyEdit;
class CBuddyComboBox;

class CNewListCtrl : public CListCtrl
{
	DECLARE_DYNAMIC(CNewListCtrl)

public:
	CNewListCtrl();
	virtual ~CNewListCtrl();

	//设置header背景，由listctrl拥有该bitmap
	void SetHeaderBackImage(HBITMAP hBmp);
	void SetHeaderBackColor(COLORREF cBack);
	void SetHeaderTextColor(COLORREF cHeaderText);
	void SetHeaderArrowColor(COLORREF cArrow);
	void SetOddRowBackColor(COLORREF cOdd);
	void SetEvenRowBackColor(COLORREF cEven);
	void SetSelectedRowBackColor(COLORREF cSelected);
	void SetVerticalLineColor(COLORREF cLine);
	void SetHorizontalLineColor(COLORREF cLine);
	//void SetSelectedRowBottomLineColor(COLORREF cLine);
	void SetColumSortType(int nColumn, ColumnSortType colSortType);
	void SetSortFun(PFNLVCOMPARE sortFun);
	void SetColumnControlType(int nColumn, ColumnControlType colCtrlType); // 目前仅支持一列为CheckBox
	void SetBuddyEdit(CBuddyEdit* pBuddyEdit);
	void SetBuddyComboBox(CBuddyComboBox* pBuddyCombo);
	void SetHeaderFont(CFont& font);
	void SetListFont(CFont& font); // 不要用CListCtrl::SetFont
	int GetSortedColumn();
	virtual BOOL SetTextColor(COLORREF cText);

	//以下函数必须listctrl创建成功才能调用,否则会挂
	void SetHeaderHeight(int nHeaderHeight); // 如果高度为0，相当于隐藏Header
	void SetRowHeight(int nRowHeight);
	int GetRowHeight(); //获取行高，和SetRowHeight的值一样
	int GetRealRowHeight(); //获取真正行高，可能和SetRowHeight的值不一样
	void SetItemImage(int nItem, HICON hIcon, BOOL bInvalidate = FALSE); //仅支持对第一列设置图标，CNewListCtrl不负责删除hIcon
	HICON GetItemImage(int nItem);
	virtual BOOL SetCheck(int nItem, BOOL bCheck = TRUE);
	virtual BOOL GetCheck(int nItem);
	virtual BOOL SetItemData(int nItem, DWORD_PTR dwData);
	virtual DWORD_PTR GetItemData(int nItem);
	virtual BOOL DeleteItem(int nItem);
	virtual BOOL DeleteAllItems();
	virtual CNewHeaderCtrl* GetHeaderCtrl();
	void SortColumn(int nColumn, ColumnSortState state);
	static int CALLBACK ListCompare(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);

public:
	//自定义列表行颜色设置
	//添加自定义颜色
	BOOL AddCustomColor(int id, int nColumn, CString strTxt, COLORREF bgColor, COLORREF fontColor=RGB(0,0,0))
	{
		if (id > MAX_CUSTOM_COLOR-1)
			return FALSE;

		CustomColor cc(id, nColumn, strTxt, bgColor, fontColor);
		m_vecCustomColor.emplace_back(cc);

		m_brushCustomColor[id].DeleteObject();
		m_brushCustomColor[id].CreateSolidBrush(bgColor);
		return TRUE;
	}

	//修改自定义颜色信息
	BOOL ChangeCustomColorInfo(int id, int nColumn, CString strTxt)
	{
		if (id > MAX_CUSTOM_COLOR - 1)
			return FALSE;

		for (auto& ele : m_vecCustomColor)
		{
			if (ele.GetColorID() == id)
			{
				ele.SetColorInfo(nColumn, strTxt);
			}
		}
		return TRUE;
	}

	//修改自定义颜色
	BOOL ChangeCustomBgColor(int id, COLORREF color)
	{
		if (id > MAX_CUSTOM_COLOR - 1)
			return FALSE;

		for (auto& ele : m_vecCustomColor)
		{
			if (ele.GetColorID() == id)
			{
				ele.SetBgColor(color);
				m_brushCustomColor[id].DeleteObject();
				m_brushCustomColor[id].CreateSolidBrush(color);
			}
		}
		return TRUE;
	}

	//修改自定义字体颜色
	BOOL ChangeCustomfontColor(int id, COLORREF color)
	{
		if (id > MAX_CUSTOM_COLOR - 1)
			return FALSE;

		for (auto& ele : m_vecCustomColor)
		{
			if (ele.GetColorID() == id)
			{
				ele.SetFontColor(color);
			}
		}
		return TRUE;
	}

	//清除自定义颜色
	void ClearCustomColor()
	{
		m_vecCustomColor.clear();
		for (int i=0; i< MAX_CUSTOM_COLOR; i++)
		{
			m_brushCustomColor[i].DeleteObject();
		}
	}

	//删除某一个自定义颜色
	BOOL DeleteCustomColor(int id)
	{
		if (id > MAX_CUSTOM_COLOR)
			return FALSE;

		for (auto it = m_vecCustomColor.begin(); it != m_vecCustomColor.end();)
		{
			if (it->GetColorID() == id)
			{
				it = m_vecCustomColor.erase(it);
				m_brushCustomColor[id].DeleteObject();
				continue;
			}
			++it;
		}
		return TRUE;
	}

	//获取所支持最大的自定义颜色数量
	int GetMaxCustomColor()
	{
		return nMaxCustomColor;
	}

	//设置合计行，永远在最下行显示，排序是忽略
	void SetSumItem(int nColomn, CString strTxt)
	{
		m_stSi.nColumn = nColomn;
		m_stSi.strTxt = strTxt;
		m_stSi.bEnable = TRUE;
	}

	//设置序号列
	void SetOrderColumn(int nColumn,CString strTxt)
	{
		m_orSi.nColumn = nColumn;
		m_orSi.strTxt = strTxt;
		m_orSi.bEnable = TRUE;
	}

private:
	CBrush m_brushCustomColor[MAX_CUSTOM_COLOR];//自定义颜色
	int nMaxCustomColor = MAX_CUSTOM_COLOR;
	//..

protected:
	DECLARE_MESSAGE_MAP()

	void Init();
	void OnSelectedRowChanged(int nRow);
	virtual void PreSubclassWindow();
	//virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void DrawSubItem(CDC *pDC, int nItem, int nSubItem, CRect& rSubItem, bool bSelected, bool bFocus);
	virtual void DrawRemainSpace(LPNMLVCUSTOMDRAW lpnmcd);
	virtual void draw_row_bg(CDC *pDC, CRect& rect, bool bSelected, bool bFocus, int nRow, int nCol);
	virtual void draw_row_bg(CDC *pDC, CRect& rect, bool bSelected, bool bFocus, bool bOdd, int nCol, int nRow=-1);
	//void InvalidateItem(int nItem);

	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnHdnItemchanged(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnHdnEndTrack(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnHdnDividerDblClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnEndScroll(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnColumnclick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

private:
	struct NItemData
	{
		BOOL bCheck;
		HICON hIcon;
		DWORD_PTR dwData;
		NItemData()
			: bCheck(FALSE)
			, hIcon(NULL)
			, dwData(NULL)
		{
		}
	};
	CNewHeaderCtrl m_header;
	CFont m_font;
	int m_nRowHeight;
	int m_nRealRowHeight;
	CBrush m_brushOddBack;
	CBrush m_brushEvenBack;
	CBrush m_brushSelectedBack;
	CPen m_penVerticalLine;
	CPen m_penHorizontalLine;
	//CPen m_penSelectedRowBottomLine;
	int m_nSortColumn;
	ColumnSortState m_sortState;
	PFNLVCOMPARE m_sortFun;
	std::map<int, ColumnSortType> m_mapColumnSortType;
	std::map<int, ColumnControlType> m_mapColumnControlType;
	std::map<int, DWORD> m_mapColumnAlign;
	CBuddyEdit* m_pBuddyEdit;
	CBuddyComboBox* m_pBuddyComboBox;
	int m_nSelectedRow;
	std::vector<NItemData*> m_vecNItemData;
	COLORREF m_colorText;

	SumItem m_stSi{};
	OrderColumn m_orSi{};
	std::vector<CustomColor> m_vecCustomColor;//自定义item颜色
};

class CBuddyComboBox : public CComboBox
{
public:
	friend class CNewListCtrl;
	CBuddyComboBox();
	int GetStringIndex(CString& cstrText);
protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnCloseup();

	DECLARE_MESSAGE_MAP()
private:
	int m_nRow;
	int m_nCol;
	BOOL m_bESC;
	CNewListCtrl* m_pList;
	CString m_cstrInit;
};

class CBuddyEdit : public CEdit
{
public:
	friend class CNewListCtrl;
	CBuddyEdit();
protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	void OnCloseup();

	DECLARE_MESSAGE_MAP()
private:
	int m_nRow;
	int m_nCol;
	BOOL m_bESC;
	CNewListCtrl* m_pList;
	CString m_cstrInit;
};

