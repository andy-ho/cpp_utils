// BtnSTTestDlg.h : header file
//

#if !defined(AFX_BTNSTTESTDLG_H__8043C682_AD6E_4558_B054_588529B5318E__INCLUDED_)
#define AFX_BTNSTTESTDLG_H__8043C682_AD6E_4558_B054_588529B5318E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CBtnSTTestDlg dialog

#include "BtnST.h"
class CBtnSTTestDlg : public CDialog
{
// Construction
public:
	CButtonST m_btntest;
	CBtnSTTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CBtnSTTestDlg)
	enum { IDD = IDD_BTNSTTEST_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBtnSTTestDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CBtnSTTestDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BTNSTTESTDLG_H__8043C682_AD6E_4558_B054_588529B5318E__INCLUDED_)
