#pragma once
#include <iostream>
#include <comdef.h>
#include <conio.h>
#include "../based/except.h"
#import "c:\Program Files\Common Files\System\ADO\msado15.dll" \
              rename("EOF", "EndOfFile")

namespace sqler
{

	class ado_session
	{
		typedef ADODB::_RecordsetPtr	RecPtr;
		typedef ADODB::_ConnectionPtr	CnnPtr;

	public:
		ado_session()
		{
			CoInitialize(NULL);
			m_Cnn = NULL;
			m_Rec = NULL;
			m_ErrStr = "NULL POINTER";
		}

		~ado_session()
		{
			close();
			m_Rec.Release();
			m_Cnn.Release();
			CoUninitialize();
		}


		bool open(const char* CnnStr)
		{
			try
			{
				HRESULT hr;
				hr = m_Cnn.CreateInstance(__uuidof(ADODB::Connection));
				m_Cnn->Open(CnnStr, "", "", NULL);
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		bool open_ex(int Mode, char* CmdStr)
		{
			if (m_Cnn == NULL)
			{
				m_Rec = NULL;
				m_ErrStr = "Invalid Connection";
				return 0;
			}
			m_Rec = NULL;
			try
			{
				//t_Rec->putref_ActiveConnection(m_Cnn);
				//vtMissing<<-->>_variant_t((IDispatch *) m_Cnn, true)
				m_Rec.CreateInstance(__uuidof(ADODB::Recordset));
				m_Rec->Open(CmdStr, _variant_t((IDispatch *)m_Cnn, true), ADODB::adOpenStatic, ADODB::adLockOptimistic, Mode);
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		// 	bool execute(char* CmdStr)
		// 	{
		// 		try
		// 		{
		// 			m_Cnn->Execute(CmdStr, NULL, 1);
		// 		}
		// 		catch (_com_error& e)
		// 		{
		// 			dump_error(e);
		// 			return false;
		// 		}
		// 
		// 		m_ErrStr = "Success";
		// 		return true;
		// 	}

		bool execute(char* CmdStr)
		{
			m_Rec = NULL;
			try
			{
				m_Rec = m_Cnn->Execute(CmdStr, NULL, 1);
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		bool close()
		{
			if (m_Cnn == NULL || !is_open())
			{
				return false;
			}

			m_Cnn->Close();

			return true;
		}

		bool is_open()
		{
			try
			{
				return (m_Cnn != NULL && (m_Cnn->State & ADODB::adStateOpen));
			}
			catch (_com_error &e)
			{
				dump_error(e);
			}
			return false;
		}


		int is_eof()
		{
			int rs;
			if (m_Rec == NULL)
			{
				m_ErrStr = "Invalid Record";
				return -1;
			}
			try {
				rs = m_Rec->EndOfFile;
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return -2;
			}

			m_ErrStr = "Success";
			return rs;
		}

		HRESULT move_next()
		{
			HRESULT hr;
			try
			{
				hr = m_Rec->MoveNext();
			}
			catch (_com_error &e)
			{
				dump_error(e);
				//m_Rec=NULL;
				return -2;
			}
			m_ErrStr = "Success";
			return hr;
		}

		HRESULT move_previous()
		{
			HRESULT hr;
			try
			{
				hr = m_Rec->MovePrevious();
			}
			catch (_com_error &e)
			{
				dump_error(e);
				//m_Rec=NULL;
				return -2;
			}
			m_ErrStr = "Success";
			return hr;
		}

		HRESULT move_first()
		{
			HRESULT hr;
			try
			{
				hr = m_Rec->MoveFirst();
			}
			catch (_com_error &e)
			{
				dump_error(e);
				//m_Rec=NULL;
				return -2;
			}
			m_ErrStr = "Success";
			return hr;
		}

		HRESULT move_last()
		{
			HRESULT hr;
			try
			{
				hr = m_Rec->MoveLast();
			}
			catch (_com_error &e)
			{
				dump_error(e);
				//m_Rec=NULL;
				return -2;
			}
			m_ErrStr = "Success";
			return hr;
		}

		bool get(char* FieldName, char* FieldValue)
		{
			try
			{
				FieldValue = NULL;
				_variant_t  vtValue;
				vtValue = m_Rec->Fields->GetItem(FieldName)->GetValue();
				ENSURE(VT_NULL != vtValue.vt)(FieldName).warn(-1, "查询结果为NULL！");
				sprintf(FieldValue, "%s", (LPCSTR)((_bstr_t)vtValue.bstrVal));
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}
			catch (...)
			{
				m_ErrStr = "查询结果为NULL！";
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		bool get(char* FieldName, std::string& FieldValue)
		{
			try
			{
				FieldValue.clear();
				_variant_t  vtValue;
				vtValue = m_Rec->Fields->GetItem(FieldName)->GetValue();
				ENSURE(VT_NULL != vtValue.vt)(FieldName).warn(-1, "查询结果为NULL！");
				FieldValue = (LPCSTR)((_bstr_t)vtValue.bstrVal);
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}
			catch (...)
			{
				m_ErrStr = "查询结果为NULL！";
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		bool get(char* FieldName, int& FieldValue)
		{
			try
			{
				FieldValue = 0;
				_variant_t  vtValue;
				vtValue = m_Rec->Fields->GetItem(FieldName)->GetValue();
				ENSURE(VT_NULL != vtValue.vt)(FieldName).warn(-1, "查询结果为NULL！");
				FieldValue = vtValue.intVal;
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}
			catch (...)
			{
				m_ErrStr = "查询结果为NULL！";
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		bool get(char* FieldName, float& FieldValue)
		{
			try
			{
				FieldValue = 0.0;
				_variant_t  vtValue;
				vtValue = m_Rec->Fields->GetItem(FieldName)->GetValue();
				ENSURE(VT_NULL != vtValue.vt)(FieldName).warn(-1, "查询结果为NULL！");
				FieldValue = vtValue.fltVal;
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}
			catch (...)
			{
				m_ErrStr = "查询结果为NULL！";
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		bool get(char* FieldName, double& FieldValue)
		{
			try
			{
				FieldValue = 0.0;
				_variant_t  vtValue;
				vtValue = m_Rec->Fields->GetItem(FieldName)->GetValue();
				ENSURE(VT_NULL != vtValue.vt)(FieldName).warn(-1, "查询结果为NULL！");
				FieldValue = vtValue.dblVal;
				//GetDec(vtValue,FieldValue,3);
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}
			catch (...)
			{
				m_ErrStr = "查询结果为NULL！";
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		bool get(char* FieldName, long long& FieldValue)
		{
			try
			{
				FieldValue = 0;
				_variant_t  vtValue;
				vtValue = m_Rec->Fields->GetItem(FieldName)->GetValue();
				ENSURE(VT_NULL != vtValue.vt)(FieldName).warn(-1, "查询结果为NULL！");
				FieldValue = vtValue.llVal;
				//GetDec(vtValue,FieldValue,3);
			}
			catch (_com_error& e)
			{
				dump_error(e);
				return false;
			}
			catch (...)
			{
				m_ErrStr = "查询结果为NULL！";
				return false;
			}

			m_ErrStr = "Success";
			return true;
		}

		std::string get_err_str()
		{
			return m_ErrStr;
		}

	private:
		void dump_error(_com_error& e)
		{
			char em[256] = "";
			_bstr_t bstrSource(e.Source());
			_bstr_t bstrDescription(e.Description());
			m_ErrStr = "\n\tMAdoDataBase Error:";
			sprintf(em, "\n\tCode = %08lx", e.Error());
			m_ErrStr += em;
			m_ErrStr += "\n\tCode meaning = ";
			m_ErrStr += (char*)e.ErrorMessage();
			m_ErrStr += "\n\tSource = ";
			m_ErrStr += (LPCSTR)bstrSource;
			m_ErrStr += "\n\tDescripiion = ";
			m_ErrStr += (LPCSTR)bstrDescription;
		}

	public:
		CnnPtr m_Cnn;
		RecPtr m_Rec;
		std::string m_ErrStr;
	};
}
