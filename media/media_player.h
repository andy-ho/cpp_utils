#pragma once
#include <windows.h>
#include <mmsystem.h>
#include <tchar.h>
#include "../based/os.h"

#pragma comment(lib,"winmm.lib")


class media_player
{
public:
	MCIERROR mci_play_mp3()
	{
		hEvent = CreateEvent(NULL,FALSE,FALSE,NULL);
		MCI_OPEN_PARMS mciOpen = {0};
		MCI_PLAY_PARMS mciPlay = {0};

		MCIERROR mciError = 0;

		while (true)
		{
			WaitForSingleObject(hEvent, INFINITE);
			if (playPath)
			{
				mciError = mciSendCommand(mciOpen.wDeviceID, MCI_CLOSE, 0, (DWORD)&mciPlay);
				if (mciError)
				{
					//printf("send MCI_PLAY command failed\n");
				}
				mciOpen.lpstrDeviceType = _T("mpegvideo");
				mciOpen.lpstrElementName = playPath;
				playPath = NULL;

				mciError = mciSendCommand(0, MCI_OPEN, MCI_OPEN_TYPE | MCI_OPEN_ELEMENT, (DWORD)&mciOpen);
				if (mciError)
				{
					wchar_t buf[128] = { 0 };
					mciGetErrorString(mciError, buf, 128);
				}

				mciError = mciSendCommand(mciOpen.wDeviceID, MCI_PLAY, 0, (DWORD)&mciPlay);
				if (mciError)
				{
					//printf("send MCI_PLAY command failed\n");
				}
			}
		}
		CloseHandle(hEvent);
	}
	
	void play_mp3(wchar_t *pName, BOOL bFullPath = FALSE)
	{
		if (bFullPath)
			set_play_path(pName);
		else if (get_mp3_path(pName, mTmpPath))
			set_play_path(mTmpPath);
	}

private:
	bool get_mp3_path(const wchar_t *name, wchar_t *pPath, int nBufflen = MAX_PATH)
	{
		if (name == NULL || pPath == NULL)
			return FALSE;
		wchar_t dir[MAX_PATH], *p;
		wcscpy_s(dir, based::os::get_exe_path_without_backslash<wchar_t>().c_str());
		p = wcsrchr(dir, '\\');
		if (p)
		{
			*p = 0;
			swprintf_s(pPath, nBufflen, _T("%s\\mp3\\%s"), dir, name);
			return true;
		}
		return false;
	}

	void set_play_path(wchar_t* path)
	{
		SetEvent(hEvent);
		playPath = path;
	}
private:
	wchar_t* playPath = NULL;
	wchar_t mTmpPath[MAX_PATH];
	HANDLE hEvent;
};

// int main()
// {
// 	//����mp3�߳�
// 	std::unique_ptr<media_player> meida_player_ptr;
// 	meida_player_ptr = std::make_unique<media_player>();
// 	std::thread(&media_player::mci_play_mp3, meida_player_ptr.get()).detach();
// 
// 	meida_player_ptr->play_mp3(_T("loading_pay.mp3"));
// 
// 	while (1)
// 	{
// 		Sleep(100);
// 	}
// 	return 0;
// }