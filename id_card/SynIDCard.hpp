#pragma once
/*新中新身份证读卡器 DKQ-116D*/
#include "stdafx.h"
#include "afxmt.h"
#include "SynPublic.h"
#pragma comment (lib, "SynIDCardAPI.lib")

namespace based{
class SynIDCard
{
	public:
		enum SynIDError
		{
			SYN_PORT_NO_FOUND = -5,
			SYN_PORT_OPEN_ERROR,
			SYN_READ_ERROR,
			SYN_READ_OK = 0,
		};
	
		int ReadIDCard(IDCardData *pIDCardData)
		{
			mCriticalSection_.Lock();

			int nReturn = SYN_READ_ERROR;
			int iPort = Syn_FindReader();
			if (iPort == 0)
			{
				nReturn = SYN_PORT_NO_FOUND; //没有找到身份证读卡器
			}
			else//iPort>100:USB, 否则是com
			{
				int nRet = 0;
				char ctmp[256] = { 0 };
				Syn_SetPhotoPath(1, ctmp);//照片存为在前路径
				unsigned char pucIIN[4];
				unsigned char pucSN[8];

				Syn_SetSexType(1);
				Syn_SetNationType(1);
				Syn_SetBornType(2);
				Syn_SetUserLifeBType(3);
				Syn_SetUserLifeEType(4, 1);

				nRet = Syn_OpenPort(iPort);
				if (nRet == 0)
				{
					if (Syn_SetMaxRFByte(iPort, 80, 0) == 0)
					{
						Syn_StartFindIDCard(iPort, pucIIN, 0);
						Syn_SelectIDCard(iPort, pucSN, 0);
						if (Syn_ReadMsg(iPort, 0, pIDCardData) == 0)
						{
							nReturn = SYN_READ_OK;
						}
						else
						{
							nReturn = SYN_READ_ERROR; //"读取身份证信息错误!"
						}
					}
				}
				else
				{
					nReturn = SYN_PORT_OPEN_ERROR; //"打开端口错误"
				}
				Syn_ClosePort(iPort);
			}
			mCriticalSection_.Unlock();
			return nReturn;
		}
	public:
		CCriticalSection mCriticalSection_;
	};
}
