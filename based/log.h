#pragma once
#include <iostream>
#include <time.h>
#include <stdio.h>
#include "os.h"
#include "../easyloggingpp/easylogging++.h"

namespace based
{
	class log
	{
	public:
		//需要初始化log： INITIALIZE_EASYLOGGINGPP
		static void init_log(bool to_standard_output = false)
		{
			time_t t = time(0);
			char t_[64]{}; strftime(t_, sizeof(t_), "%Y%m%d%H%M%S", localtime(&t));

			std::stringstream text_;
			text_ << based::os::get_exe_path<char>() << "_\\" << t_ << ".log";
			based::os::make_dir_recursive(text_.str().c_str(), true);

			std::stringstream log_config_text;
			log_config_text
				<< "*GLOBAL:\n"
// 				<< "FORMAT = \"%datetime %level [%file : %line] %msg\"\n"
				<< "FILENAME = \"" << text_.str() << "\"\n"
				<< "MAX_LOG_FILE_SIZE = 10485760 ##10M\n"
				<< "TO_STANDARD_OUTPUT = " << to_standard_output << "\n";

			el::Configurations log_config;
			log_config.setToDefault();
			log_config.parseFromText(log_config_text.str());
			el::Loggers::reconfigureAllLoggers(log_config);

			el::Loggers::addFlag(el::LoggingFlag::StrictLogFileSizeCheck);
			el::Helpers::installPreRollOutCallback(log::rolloutHandler);
		}

		static void uninit_log()
		{
			//注销回调函数  
			el::Helpers::uninstallPreRollOutCallback();
		}

		static void rolloutHandler(const char* filename, std::size_t size)
		{
			//备份日志
			static unsigned long idx = 0;
			std::stringstream bak_file_name;
			bak_file_name << filename << "."<< idx++ <<".bak";
			CopyFileA(filename, bak_file_name.str().c_str(), FALSE);
		}
	};
}
