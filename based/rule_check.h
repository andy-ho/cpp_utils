#pragma once
#include <string>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <regex>

namespace based
{
	class rule_check
	{
	#pragma warning(disable:4996)
	public:
		//身份证号码校验;  return: true:成功！ false：失败！
		static bool id_verify(std::string idcard, int* gender = nullptr/*0: 女 1::表示男*/)
		{
			//把15位身份证升级为18位
			if (idcard.length() == 15)
			{
				idcard = uptoeighteen(idcard);
			}

			if (idcard.length() != 18)
			{
				false;
			}

			//1-17必须是数字
			for (int i = 0; i < 17; ++i)
			{
				if (idcard[i] < '0' || idcard[i] > '9')
				{
					false;
				}
			}

			//最后一位(第18位)为校验码，检查checksum值
			char verify = toupper(idcard[17]);
			if (verify != checksum(idcard))
			{
				return false;
			}

			//第1-2位为省级行政区划代码，[11, 65] (第一位华北区1，东北区2，华东区3，中南区4，西南区5，西北区6)
			std::string strProvince = idcard.substr(0, 2);
			int nProvince = atoi(strProvince.c_str());
			if (nProvince < 11 || nProvince > 65)
			{
				return false;
			}

			//第3-4为为地级行政区划代码，第5-6位为县级行政区划代码因为经常有调整，这块就不做校验

			//第7-10位为出生年份；//第11-12位为出生月份 //第13-14为出生日期
			if (!checkdate(idcard.substr(6, 8)))
			{
				return false;
			}

			//第15-17位为顺序码，表示在同一地址码所标识的区域范围内，对同年、同月、同日出生的人编定的顺序号，顺序码的奇数分配给男性，偶数分配给女性
			std::string strSeq = idcard.substr(14, 3);
			int nSeq = atoi(strSeq.c_str());
			if (gender)
				*gender = nSeq % 2;

			return true;
		}


		//手机号校验；return true:成功！ false:失败！
		static bool phone_num_verify(std::string phone_num)
		{
			if (std::regex_match(phone_num, std::regex("^1[34578][0-9]{9}$")))
				return true;
			else
				return false;
		}

		//日期校验：YYYYMMDD 或 YYYY-MM-DD；return true:成功！ false:失败！
		static bool date_verify(std::string date)
		{
			if (std::regex_match(date, std::regex("^(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-?(((0[13578]|1[02])-?(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-?(0[1-9]|[12][0-9]|30))|(02-?(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-?02-?29)$")))
				return true;
			else
				return false;
		}
		//Ip校验：return true:成功！ false:失败！
		static bool ip_verify(std::string ip)
		{
			if (std::regex_match(ip, std::regex("^(25[0-4]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[1-9])[.](25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])[.](25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])[.](25[0-4]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[1-9])$")))
				return true;
			else
				return false;
		}

	private:
		//ID CARD
		static bool checkdate(const std::string& strDate)
		{
			std::string strYear = strDate.substr(0, 4);
			int nYear = atoi(strYear.c_str());

			time_t timep;
			time(&timep);
			struct tm *p = gmtime(&timep);
			if (p)
			{
				if (nYear < 1900 || nYear > 1900 + p->tm_year)
				{
					return false;
				}

				if (atoi(strDate.c_str()) > ((1900 + p->tm_year) * 10000 + p->tm_mon * 100 + p->tm_mday))
				{
					return false;
				}
			}

			std::string strMon = strDate.substr(4, 2);
			int nMon = atoi(strMon.c_str());
			if (nMon <= 0 || nMon > 12)
			{
				return false;
			}

			//月份天数表
			int days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
			bool bLeapYear = (nYear % 4 == 0 && nYear % 100 != 0) || (nYear % 400 == 0);
			if (bLeapYear)
			{
				days[1] = 29;
			}
			std::string strDay = strDate.substr(6, 2);
			int nDay = atoi(strDay.c_str());
			if (nDay > days[nMon - 1] || nDay <= 0)
			{
				return false;
			}

			return true;
		}

		//计算校验码
		static char checksum(std::string eightcardid)
		{
			// wi =2(n-1)(mod 11) 
			int wi[18] = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1 };
			// verify digit 
			char vi[11] = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };

			int remaining = 0;
			int ai[18];

			if (eightcardid.length() == 18)
			{
				eightcardid = eightcardid.substr(0, 17);
			}

			if (eightcardid.length() == 17)
			{
				int sum = 0;
				for (int i = 0; i < 17; i++)
				{
					std::string k = eightcardid.substr(i, 1);
					ai[i] = atoi(k.c_str());
				}

				for (int i = 0; i < 17; i++)
				{
					sum = sum + wi[i] * ai[i];
				}
				remaining = sum % 11;
			}

			return vi[remaining];
		}

		//把15位身份证升级到18位身份证号码 
		static std::string uptoeighteen(const std::string& fifteencardid)
		{
			std::string eightcardid = fifteencardid.substr(0, 6);
			//加上年月日
			eightcardid = eightcardid + "19";
			eightcardid = eightcardid + fifteencardid.substr(6);
			//加上校验码
			eightcardid.push_back(checksum(eightcardid));
			return eightcardid;
		}
	};
	//ID CARD..


}