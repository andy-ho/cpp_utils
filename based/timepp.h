//ʱ��ת����
#pragma once
#include <time.h>
namespace based
{
	class timepp
	{
	public:
		static int get_timezone()
		{
			time_t time_utc;
			struct tm tm_local;

			// Get the UTC time  
			time(&time_utc);

			// Get the local time  
			// Use localtime_r for threads safe  
			localtime_s(&tm_local, &time_utc);


			// Change tm to time_t   
			//time_t time_local;
			//time_local = mktime(&tm_local);

			// Change it to GMT tm  
			struct tm tm_gmt;
			gmtime_s(&tm_gmt, &time_utc);

			int time_zone = tm_local.tm_hour - tm_gmt.tm_hour;
			if (time_zone < -12) {
				time_zone += 24;
			}
			else if (time_zone > 12) {
				time_zone -= 24;
			}
			return time_zone;
		}

		//pFormat = "";
		//"%Y-%m-%d %H:%M:%S"
		static std::string get_time_str(char* pFormat = "%Y-%m-%d %H:%M:%S")
		{
			time_t timep;
			time(&timep);
			char tmp[64];
			strftime(tmp, sizeof(tmp), pFormat, localtime(&timep));
			return tmp;
		}
	};
}