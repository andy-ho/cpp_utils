//一个跟系统有关的常用功能api的封装。

#pragma once

#if defined(_MSC_VER) && _MSC_VER < 1800
	#error "Compiler need to support c++11, please use vs2013 or above, vs2015 e.g."
#endif

#include "def.h"
#include "headers_dependency.h"
#include "string_more.h"
#include <tchar.h>
#include <string.h>
#include <winioctl.h>
#include <TlHelp32.h>
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")

#define IOCTL_STORAGE_QUERY_PROPERTY   CTL_CODE(IOCTL_STORAGE_BASE, 0x0500, METHOD_BUFFERED, FILE_ANY_ACCESS)


namespace based
{
	//操作系统相关api的封装
	class os
	{
	#ifdef WIN32
	public:
		//获取程序版本号。格式：【V%d.%d.%d.%04d】
		//linker->input 中添加version.lib
		static tstring GetApplicationVersion()
		{
			TCHAR szFullPath[MAX_PATH];
			DWORD dwVerInfoSize = 0;
			DWORD dwVerHnd;
			VS_FIXEDFILEINFO * pFileInfo;

			GetModuleFileName(NULL, szFullPath, sizeof(szFullPath));
			dwVerInfoSize = GetFileVersionInfoSize(szFullPath, &dwVerHnd);
			tstring strVersion = _T("V0.0.0.0000");
			if (dwVerInfoSize)
			{
				// If we were able to get the information, process it:
				HANDLE  hMem;
				LPVOID  lpvMem;
				unsigned int uInfoSize = 0;
				hMem = GlobalAlloc(GMEM_MOVEABLE, dwVerInfoSize);
				lpvMem = GlobalLock(hMem);
				GetFileVersionInfo(szFullPath, dwVerHnd, dwVerInfoSize, lpvMem);
				::VerQueryValue(lpvMem, (LPTSTR)_T("\\"), (void**)&pFileInfo, &uInfoSize);
				int ret = GetLastError();
				WORD m_nProdVersion[4]{};

				// Product version from the FILEVERSION of the version info resource 
				m_nProdVersion[0] = HIWORD(pFileInfo->dwProductVersionMS);
				m_nProdVersion[1] = LOWORD(pFileInfo->dwProductVersionMS);
				m_nProdVersion[2] = HIWORD(pFileInfo->dwProductVersionLS);
				m_nProdVersion[3] = LOWORD(pFileInfo->dwProductVersionLS);
				TCHAR v_[128]{}; _stprintf(v_, _T("V%d.%d.%d.%04d"), m_nProdVersion[0], m_nProdVersion[1], m_nProdVersion[2], m_nProdVersion[3]);
				strVersion = v_;
				GlobalUnlock(hMem);
				GlobalFree(hMem);
			}
			return strVersion;
		}

		//得到exe全路径，比如d:\aa\bb\cc.exe
		template<typename char_set_t>
		static typename std::enable_if<std::is_same<char_set_t, char>::value, std::string>::type get_exe_path(std::string split="")
		{
			char* exe_full_path = new char[MAX_PATH];
			char* exe_full_path_fix = new char[MAX_PATH];

			GetModuleFileNameA(NULL, exe_full_path, MAX_PATH);
			PathCanonicalizeA(exe_full_path_fix, exe_full_path);
			std::string str_exe_path(exe_full_path_fix);
			if (!split.empty())
			{
				based::string_more::replace_all(str_exe_path, std::string("\\"), split);
			}

			delete[] exe_full_path;
			delete[] exe_full_path_fix;
			return str_exe_path;
		}

		template<typename char_set_t>
		static typename std::enable_if<std::is_same<char_set_t, wchar_t>::value, std::wstring>::type get_exe_path(std::wstring split = L"")
		{
			wchar_t* exe_full_path = new wchar_t[MAX_PATH];
			wchar_t* exe_full_path_fix = new wchar_t[MAX_PATH];

			GetModuleFileNameW(NULL, exe_full_path, MAX_PATH);
			PathCanonicalizeW(exe_full_path_fix, exe_full_path);
			std::wstring str_exe_path(exe_full_path);
			if (!split.empty())
			{
				based::string_more::replace_all(str_exe_path, std::wstring(L"\\"), split);
			}

			delete[] exe_full_path;
			delete[] exe_full_path_fix;

			return str_exe_path;
		}

		//得到不带'\'的exe路径，比如，由d:\aa\bb\cc.exe，得到d:\aa\bb
		template<typename char_set_t>
		static typename std::enable_if<std::is_same<char_set_t, char>::value, std::string>::type get_exe_path_without_backslash(std::string split="")
		{
			char* exe_full_path = new char[MAX_PATH];
			char* exe_full_path_fix = new char[MAX_PATH];

			GetModuleFileNameA(NULL, exe_full_path, MAX_PATH);
			PathCanonicalizeA(exe_full_path_fix, exe_full_path);
			std::string str_exe_path(exe_full_path_fix);
			str_exe_path.erase(str_exe_path.rfind(('\\')));
			if (!split.empty())
				based::string_more::replace_all(str_exe_path, std::string("\\"), split);

			delete[] exe_full_path;
			delete[] exe_full_path_fix;


			return str_exe_path;
		}

		template<typename char_set_t>
		static typename std::enable_if<std::is_same<char_set_t, wchar_t>::value, std::wstring>::type get_exe_path_without_backslash(std::wstring split = L"")
		{
			wchar_t* exe_full_path = new wchar_t[MAX_PATH];
			wchar_t* exe_full_path_fix = new wchar_t[MAX_PATH];

			GetModuleFileNameW(NULL, exe_full_path, MAX_PATH);
			PathCanonicalizeW(exe_full_path_fix, exe_full_path);
			std::wstring str_exe_path(exe_full_path);
			str_exe_path.erase(str_exe_path.rfind((L'\\')));
			if (!split.empty())
				based::string_more::replace_all(str_exe_path, std::wstring(L"\\"), split);

			delete[] exe_full_path;
			delete[] exe_full_path_fix;

			return str_exe_path;
		}

		template<typename char_set_t>
		static bool make_dir_recursive(const char_set_t* path, bool is_file = false)
		{
			int str_len;
			if (path == NULL || (str_len=strlen(path)) == 0)
				return false;

			std::string str_path(path);
			std::replace(str_path.begin(), str_path.end(), '/', '\\');

			if (is_file == false && str_path.at(str_path.length()-1) != '\\')
				str_path += '\\';

			str_len = str_path.length();
			for (int i = 0; i < str_len; i++)
			{
				if (str_path.at(i) == '\\')
				{
					std::string& sub_str = str_path.substr(0, i);
					if(!PathFileExistsA(sub_str.c_str()))
						CreateDirectoryA(sub_str.c_str(), NULL);
				}	
			}
			return true;
		}

		template<>
		static bool make_dir_recursive(const wchar_t* path, bool is_file)
		{
			int str_len;
			if (path == NULL || (str_len = wcslen(path)) == 0)
				return false;

			std::wstring str_path(path);
			std::replace(str_path.begin(), str_path.end(), L'/', L'\\');

			if (is_file == false && str_path.at(str_path.length() - 1) != L'\\')
				str_path += L'\\';

			str_len = str_path.length();
			for (int i = 0; i < str_len; i++)
			{
				if (str_path.at(i) == L'\\')
				{
					std::wstring& sub_str = str_path.substr(0, i);
					if (!PathFileExistsW(sub_str.c_str()))
						CreateDirectoryW(sub_str.c_str(), NULL);
				}
			}
			return true;
		}

		//volume：c d e f...分区；
		//encode:volumeid加密秘钥，采用id与该值异或操作；
		//返回该分区的id号；每次格式化分区后id会变化；
		static std::string get_volume_id(std::string volume, DWORD encode=0)
		{
			std::string volume_id;

			char     m_Volume[256];//卷标名   
			char     m_FileSysName[256];
			DWORD   m_SerialNum;//序列号   
			DWORD   m_FileNameLength;
			DWORD   m_FileSysFlag;

			volume.append(":\\");
			::GetVolumeInformationA(volume.c_str(),
				m_Volume,
				256,
				&m_SerialNum,
				&m_FileNameLength,
				&m_FileSysFlag,
				m_FileSysName,
				256);

			if (encode != 0)
			{
				m_SerialNum ^= encode;
			}

			char ser[10]{};
			sprintf_s(ser, "%08X", m_SerialNum);
			volume_id.append(ser);
			return volume_id;
		}

		//获取硬盘序列号
		static std::string get_physical_driver_sn(int driver=0, int full_sn=true, int encode=0, int start_pos=8)
		{
			int done = FALSE;
			std::string str_serial_number;

			HANDLE hPhysicalDriveIOCTL = 0;

			//  Try to get a handle to PhysicalDrive IOCTL, report failure
			//  and exit if can't.
			char driveName[256];

			sprintf(driveName, "\\\\.\\PhysicalDrive%d", driver);

			//  Windows NT, Windows 2000, Windows XP - admin rights not required
			hPhysicalDriveIOCTL = CreateFileA(driveName,
				0,
				FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
				OPEN_EXISTING, 0, NULL);
			if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE)
			{
			}
			else
			{
				STORAGE_PROPERTY_QUERY query;
				DWORD cbBytesReturned = 0;
				char buffer[10000];

				memset((void *)& query, 0, sizeof(query));
				query.PropertyId = StorageDeviceProperty;
				query.QueryType = PropertyStandardQuery;

				memset(buffer, 0, sizeof(buffer));

				char serialNumber[1000]{};
				char modelNumber[1000]{};
				char vendorId[1000]{};
				char productRevision[1000]{};

				if (DeviceIoControl(hPhysicalDriveIOCTL, IOCTL_STORAGE_QUERY_PROPERTY,
					&query,
					sizeof(query),
					&buffer,
					sizeof(buffer),
					&cbBytesReturned, NULL))
				{
					STORAGE_DEVICE_DESCRIPTOR * descrip = (STORAGE_DEVICE_DESCRIPTOR *)& buffer;
					flipAndCodeBytes(buffer,
						descrip->VendorIdOffset,
						0, vendorId);
					flipAndCodeBytes(buffer,
						descrip->ProductIdOffset,
						0, modelNumber);
					flipAndCodeBytes(buffer,
						descrip->ProductRevisionOffset,
						0, productRevision);
					flipAndCodeBytes(buffer,
						descrip->SerialNumberOffset,
						1, serialNumber);

					if (
						//  serial number must be alphanumeric
						//  (but there can be leading spaces on IBM drives)
						(isalnum(serialNumber[0]) || isalnum(serialNumber[19])))
					{
						str_serial_number = serialNumber;
						done = TRUE;
					}

				}
				else
				{
					DWORD err = GetLastError();
#ifdef PRINTING_TO_CONSOLE_ALLOWED
					printf("\nDeviceIOControl IOCTL_STORAGE_QUERY_PROPERTY error = %d\n", err);
#endif
				}

				if (full_sn==false)
				{
					if (start_pos > 15)
					{
						start_pos = 15;
					}
					DWORD dwSn = 0;
					dwSn = serialNumber[start_pos] + (DWORD)(serialNumber[start_pos + 1] << 8) +
						(DWORD)(serialNumber[start_pos + 2] << 16) + (DWORD)(serialNumber[start_pos + 3] << 24);

					if (encode != 0)
					{
						dwSn ^= encode;
					}
					char ser[10]{};
					sprintf_s(ser, "%08X", dwSn);
					str_serial_number = ser;
				}
				CloseHandle(hPhysicalDriveIOCTL);
			}
			return str_serial_number;
		}

		//获取最后一块网卡地址.
		//full_mac == true 长度6byte，不能使用encode加密，start_pos不可用；
		//full_mac == false 长度4byte，并且可使用encode加密，可指定起始位置start_pos(默认为0)；
		static std::string get_mac_address(bool full_mac=true, DWORD encode=0, int start_pos=0, int adapter_type = MIB_IF_TYPE_ETHERNET)
		{
			DWORD dwMACaddress = 0;
			std::string str_mac_addr("5A5A5AA5A5A5");

			IP_ADAPTER_INFO AdapterInfo[16];       // Allocate information
												   // for up to 16 NICs
			DWORD dwBufLen = sizeof(AdapterInfo);  // Save memory size of buffer
		
			DWORD dwStatus = GetAdaptersInfo(      // Call GetAdapterInfo
				AdapterInfo,                 // [out] buffer to receive data
				&dwBufLen);                  // [in] size of receive data buffer
			assert(dwStatus == ERROR_SUCCESS);  // Verify return value is
												// valid, no buffer overflow
		
			PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo; // Contains pointer to
														 // current adapter info
			do
			{
				if (pAdapterInfo->Type != adapter_type)
				{
					pAdapterInfo = pAdapterInfo->Next;    // Progress through linked list
					continue;
				}
				if (full_mac)
				{
					str_mac_addr = format_mac_address(pAdapterInfo->Address, false);
				}
				else
				{
					if (dwMACaddress == 0)
					{
						//总长度为6byte,full_mac为false时，取4byte，最大起始位置为2.
						if (start_pos > 2)
						{
							start_pos = 2;
						}

						dwMACaddress = pAdapterInfo->Address[5 - start_pos] + (DWORD)(pAdapterInfo->Address[4 - start_pos] << 8) +
							(DWORD)(pAdapterInfo->Address[3 - start_pos] << 16) + (DWORD)(pAdapterInfo->Address[2 - start_pos] << 24);

						if (encode != 0)
						{
							dwMACaddress ^= encode;
						}

						char ser[10]{};
						sprintf_s(ser, "%08X", dwMACaddress);
						str_mac_addr = ser;
					}
				}
				pAdapterInfo = pAdapterInfo->Next;    // Progress through linked list
			} while (pAdapterInfo);                    // Terminate if last adapter
		
			return str_mac_addr;
		}

		/*processNameToKill：进程名*/
		/*killAll：TRUE只杀一个，FALSE杀所有进程名等于processNameToKill的进程； */
		/*killParentProcess：是否杀父进程，TRUE为杀*/
		/*需管理员权限*/
		static BOOL kill_process(LPCTSTR process_name_to_kill, BOOL kill_all, BOOL kill_parent_process)
		{
			PROCESSENTRY32 pe32;
			pe32.dwSize = sizeof(pe32);
			HANDLE hpro = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
			if (hpro == INVALID_HANDLE_VALUE)
				return FALSE;

			BOOL killFail = FALSE;
			BOOL procExists = Process32First(hpro, &pe32);
			while (procExists)
			{
				if (_tcscmp(pe32.szExeFile, process_name_to_kill) == 0)
				{
					DWORD proid = pe32.th32ProcessID;
					DWORD parent_id = pe32.th32ParentProcessID;
					HANDLE hprocess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, proid);
					if (hprocess != NULL)
					{
						if (::TerminateProcess(hprocess, 0) == 0)
							killFail = TRUE;
						::CloseHandle(hprocess);
					}

					if (kill_parent_process)
					{
						HANDLE hParentprocess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, parent_id);
						if (hParentprocess != NULL)
						{
							::TerminateProcess(hParentprocess, 0);
							::CloseHandle(hParentprocess);
						}
					}

					if (!kill_all)
						break;
				}
				procExists = ::Process32Next(hpro, &pe32);
			}

			return !killFail;
		}


	private:
		//  function to decode the serial numbers of IDE hard drives
		//  using the IOCTL_STORAGE_QUERY_PROPERTY command 
		static char * flipAndCodeBytes(const char * str,
			int pos,
			int flip,
			char * buf)
		{
			int i;
			int j = 0;
			int k = 0;

			buf[0] = '\0';
			if (pos <= 0)
				return buf;

			if (!j)
			{
				char p = 0;

				// First try to gather all characters representing hex digits only.
				j = 1;
				k = 0;
				buf[k] = 0;
				for (i = pos; j && str[i] != '\0'; ++i)
				{
					char c = tolower(str[i]);

					if (isspace(c))
						c = '0';

					++p;
					buf[k] <<= 4;

					if (c >= '0' && c <= '9')
						buf[k] |= (unsigned char)(c - '0');
					else if (c >= 'a' && c <= 'f')
						buf[k] |= (unsigned char)(c - 'a' + 10);
					else
					{
						j = 0;
						break;
					}

					if (p == 2)
					{
						if (buf[k] != '\0' && !isprint(buf[k]))
						{
							j = 0;
							break;
						}
						++k;
						p = 0;
						buf[k] = 0;
					}

				}
			}

			if (!j)
			{
				// There are non-digit characters, gather them as is.
				j = 1;
				k = 0;
				for (i = pos; j && str[i] != '\0'; ++i)
				{
					char c = str[i];

					if (!isprint(c))
					{
						j = 0;
						break;
					}

					buf[k++] = c;
				}
			}

			if (!j)
			{
				// The characters are not there or are not printable.
				k = 0;
			}

			buf[k] = '\0';

			if (flip)
				// Flip adjacent characters
				for (j = 0; j < k; j += 2)
				{
					char t = buf[j];
					buf[j] = buf[j + 1];
					buf[j + 1] = t;
				}

			// Trim any beginning and end space
			i = j = -1;
			for (k = 0; buf[k] != '\0'; ++k)
			{
				if (!isspace(buf[k]))
				{
					if (i < 0)
						i = k;
					j = k;
				}
			}

			if ((i >= 0) && (j >= 0))
			{
				for (k = i; (k <= j) && (buf[k] != '\0'); ++k)
					buf[k - i] = buf[k];
				buf[k - i] = '\0';
			}

			return buf;
		}

		//即系mac地址数据
		static std::string format_mac_address(unsigned char mac_data[], bool enable_split = true)
		{
			char string[256]{};
			if (enable_split)
			{
				sprintf(string, "%02X-%02X-%02X-%02X-%02X-%02X", mac_data[0], mac_data[1],
					mac_data[2], mac_data[3], mac_data[4], mac_data[5]);
			}
			else
			{
				sprintf(string, "%02X%02X%02X%02X%02X%02X", mac_data[0], mac_data[1],
					mac_data[2], mac_data[3], mac_data[4], mac_data[5]);
			}
			return std::string(string);
		}

	#else
		#error "Please add corresponding platform's code."
	#endif
	};
}


