//这是一个字符串操作的功能增强类。 补充std::string部分功能的缺失。
//

#pragma once

#if defined(_MSC_VER) && _MSC_VER < 1800
	#error "Compiler need to support c++11, please use vs2013 or above, vs2015 e.g."
#endif

#include "headers_dependency.h"

namespace based
{
	class string_more
	{
	public:
		template<typename string_t>
		static string_t& replace_all(string_t& dest_str, const string_t& old_value, const string_t& new_value)
		{
			string_t::size_type pos(0);

			while (true) {
				if ((pos = dest_str.find(old_value, pos)) != string_t::npos)
				{
					dest_str.replace(pos, old_value.length(), new_value);
					pos += new_value.length();
				}
				else
					break;
			}
			return dest_str;
		}

		template<typename string_t>
		static int strcnt(string_t dest_str, const string_t str_to_find)
		{
			string_t::size_type pos(0);
			string_t::size_type cnt(0);
			while (true) {
				if ((pos = dest_str.find(str_to_find, pos)) != string_t::npos)
				{
					pos += str_to_find.length();
					cnt++;
				}
				else
					break;
			}
			return cnt;
		}

		template<typename string_t>
		static std::vector<string_t> split(string_t str, string_t pattern, bool include_empty_string = true)
		{
			string_t::size_type pos;
			std::vector<string_t> result;
			str += pattern;	//扩展字符串以方便操作
			int size = str.size();

			for (int i = 0; i < size; i++)
			{
				pos = str.find(pattern, i);
				if (pos < size)
				{
					string_t s = str.substr(i, pos - i);
					string_t s_(s); //临时变量，去除空格
					replace_all<string_t>(s_, " ", "");
					if (include_empty_string || !s_.empty()) //包含空字符串 || 不为空
						result.emplace_back(s);
					i = pos + pattern.size() - 1;
				}
			}
			return result;
		}

		static std::string & std_string_format(std::string & _str, const char * _Format, ...)
		{
			std::string tmp;

			va_list marker = NULL;
			va_start(marker, _Format);

			size_t num_of_chars = _vscprintf(_Format, marker);

			if (num_of_chars > tmp.capacity()) {
				tmp.resize(num_of_chars + 1);
			}

			vsprintf_s((char *)tmp.data(), tmp.capacity(), _Format, marker);

			va_end(marker);

			_str = tmp.c_str();
			return _str;
		}

	};
}

//example:
//#include "string_more.h"
// int main()
// {
// 	std::wstring str = L"ab \n ";
// 	based::string_more::replace_all<std::wstring>(str, L" ", L"");
// 	std::wcout << str;
// 
// 	return 0;
// }