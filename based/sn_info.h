#pragma once
#include "stdafx.h"

/*这些是依赖的头文件。可将这些头文件移动到stdafx.h中，以加快编译速度。--------------------------------------
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include "../../../CppProject/sharedproject/cryptopp562/randpool.h"
#include "../../../CppProject/sharedproject/cryptopp562/osrng.h"
#include "../../../CppProject/sharedproject/cryptopp562/rsa.h"
#include "../../../CppProject/sharedproject/cryptopp562/aes.h"
#include "../../../CppProject/sharedproject/cryptopp562/hex.h"
#include "../../../CppProject/sharedproject/cryptopp562/files.h"
#include "../../../CppProject/sharedproject/cryptopp562/filters.h"
#include "../../../CppProject/sharedproject/cryptopp562/modes.h"
#include "../../../CppProject/sharedproject/cryptopp562/md5.h"

#if defined (DEBUG)|| defined (_DEBUG)
#pragma comment(lib, "../../../CppProject/sharedproject/cryptopp562/Win32/Output/Debug/cryptlib.lib")
#else
#pragma comment(lib, "../../../CppProject/sharedproject/cryptopp562/Win32/Output/Release/cryptlib.lib")
#endif


#include "../../../CppProject/sharedproject/based/os.h"
#include "../../../CppProject/sharedproject/based/code_decode.h"
#include "../../../CppProject/sharedproject/encryptor/encryptor.h"

--------------------------------------------------------------------------------------------------------------*/

namespace based
{
	class sn_info
	{
	public:
		//获取机器识别码
		static std::string get_hard_code(bool use_md5 = false)
		{
			std::string hard_code = based::os::get_volume_id("c", 0x12345678);
			if (use_md5 == false)
				return hard_code;
			else
				return encryptor::Md5_::md5_string(hard_code);
		}

		//获取机器加解密码
		static std::string get_hard_key(bool use_md5 = false)
		{
			std::string hard_code = based::os::get_physical_driver_sn(0, false, 0x21211314);
			if (use_md5 == false)
				return hard_code;
			else
				return encryptor::Md5_::md5_string(hard_code);
		}

		//获取机器加解密码2
		static std::string get_hard_key2(bool use_md5 = false)
		{
			std::string hard_code = based::os::get_mac_address(false, 0xDA8EC29B);
			if (use_md5 == false)
				return hard_code;
			else
				return encryptor::Md5_::md5_string(hard_code);
		}

		//获取机器加解密码3
		static std::string get_hard_key3(bool use_md5 = false)
		{
			std::string hard_code = based::os::get_mac_address(false, 0x5211314A, 2);
			if (use_md5 == false)
				return hard_code;
			else
				return encryptor::Md5_::md5_string(hard_code);
		}

		//获取机器组合码（[8位机器码]8位机器码3[8位机器加解密码]8位机器机器码2）
		static std::string get_hard_comb_code()
		{
			std::string hard_comb_code = get_hard_code();
			hard_comb_code.append(get_hard_key3());
			hard_comb_code.append(get_hard_key());
			hard_comb_code.append(get_hard_key2());
			return hard_comb_code;
		}

		//解析组合码，获取hard_code、hard_key；
		//hard_code、hard_key为组合码中[8位机器码]、[8位机器加解密码]经过MD5加密后得到
		static void get_decoded_hard_info(std::string comb_code, std::string& hard_code, std::string& hard_key)
		{
			std::string o_hard_code(comb_code.c_str(), 0, 8);
			std::string o_hard_key(comb_code.c_str(), 16 ,8);
			hard_code = encryptor::Md5_::md5_string(o_hard_code);
			hard_key = encryptor::Md5_::md5_string(o_hard_key);
		}

		//获取加密key；
		//hard_comb_code：组合码
		//real_id：真正的ID
		//返回：加密后key，即软件SN；
		static std::string get_encoded_id(std::string hard_comb_code, std::string real_id)
		{
			std::string hard_code;
			std::string hard_key;
			get_decoded_hard_info(hard_comb_code, hard_code, hard_key);

			unsigned char key[16] = { 0x01, 0x12, 0x23, 0x44, 0x45, 0x56, 0x67, 0x78, 0x89, 0x9A, 0xAB, 0xBC, 0xCD, 0xDE, 0xEF, 0xF0 };
			unsigned char code1[16]{}, code2[16]{}, code[16]{}, key2[16]{};
			based::code_decode::str_to_hex(code1, (unsigned char*)hard_code.c_str(), hard_code.size() / 2);
			based::code_decode::str_to_hex(code2, (unsigned char*)real_id.c_str(), real_id.size() / 2);
			based::code_decode::str_to_hex(key2, (unsigned char*)hard_key.c_str(), hard_key.size() / 2);

			for (int i = 0; i < 16; i++)
			{
				code[i] = code1[i] ^ code2[i] ^ key[i] ^ key2[i];
			}
			std::string encoded_id = based::code_decode::hex_to_str((unsigned char*)code, 16).c_str();

			return encoded_id;
		}

		//获取解密ID
		//encoded_id:加密ID，软件SN
		//hard_code:硬件识别码（MD5加密后）；
		//hard_key:硬件解密码（MD5加密后）；
		//返回：真正的ID
		static std::string get_decoded_id(std::string encoded_id, std::string hard_code = "", std::string hard_key = "")
		{
			if (hard_code.empty())
			{
				hard_code = get_hard_code(true);
			}
			if (hard_key.empty())
			{
				hard_key = get_hard_key(true);
			}

			unsigned char key[16] = { 0x01, 0x12, 0x23, 0x44, 0x45, 0x56, 0x67, 0x78, 0x89, 0x9A, 0xAB, 0xBC, 0xCD, 0xDE, 0xEF, 0xF0 };
			unsigned char code1[16]{}, code2[16]{}, code[16]{}, key2[16]{};
			based::code_decode::str_to_hex(code1, (unsigned char*)hard_code.c_str(), hard_code.size() / 2);
			based::code_decode::str_to_hex(code2, (unsigned char*)encoded_id.c_str(), encoded_id.size() / 2);
			based::code_decode::str_to_hex(key2, (unsigned char*)hard_key.c_str(), hard_key.size() / 2);

			for (int i = 0; i < 16; i++)
			{
				code[i] = code1[i] ^ code2[i] ^ key[i] ^ key2[i];
			}

			std::string decoded_id = based::code_decode::hex_to_str((unsigned char*)code, 16).c_str();

			return decoded_id;
		}
	};
}
