//这是一个字符串编解码类。目前支持：url编码、解码。

#pragma once
#include<iostream>
namespace based
{
	class code_decode
	{
	public:
		static unsigned char to_hex(const unsigned char &x)
		{
			return x > 9 ? x - 10 + 'A' : x + '0';
		}

		static unsigned char from_hex(const unsigned char &x)
		{
			return isdigit(x) ? x - '0' : x - 'A' + 10;
		}

		static std::string url_encode(const char *sIn)
		{
			std::string sOut;
			for (size_t ix = 0; ix < strlen(sIn); ix++)
			{
				unsigned char buf[4];
				memset(buf, 0, 4);
				if (isalnum((unsigned char)sIn[ix]))
				{
					buf[0] = sIn[ix];
				}
				else
				{
					buf[0] = '%';
					buf[1] = to_hex((unsigned char)sIn[ix] >> 4);
					buf[2] = to_hex((unsigned char)sIn[ix] % 16);
				}
				sOut += (char *)buf;
			}
			return sOut;
		};

		static std::string url_decode(const char *sIn)
		{
			std::string sOut;
			for (size_t ix = 0; ix < strlen(sIn); ix++)
			{
				unsigned char ch = 0;
				if (sIn[ix] == '%')
				{
					ch = (from_hex(sIn[ix + 1]) << 4);
					ch |= from_hex(sIn[ix + 2]);
					ix += 2;
				}
				else if (sIn[ix] == '+')
				{
					ch = ' ';
				}
				else
				{
					ch = sIn[ix];
				}
				sOut += (char)ch;
			}

			return sOut;
		}
		
		/*
		// parameter(s): [OUT] pbDest - 输出缓冲区
		//	[IN] pbSrc - 字符串
		//	[IN] nLen - 16进制数的字节数(字符串的长度/2)
		// return value:
		// remarks : 将字符串转化为16进制数
		*/
		static void str_to_hex(unsigned char *pbDest, const unsigned char *pbSrc, int nLen)
		{
			char h1, h2;
			BYTE s1, s2;
			int i;

			for (i = 0; i < nLen; i++)
			{
				h1 = pbSrc[2 * i];
				h2 = pbSrc[2 * i + 1];

				s1 = toupper(h1) - 0x30;
				if (s1 > 9)
					s1 -= 7;

				s2 = toupper(h2) - 0x30;
				if (s2 > 9)
					s2 -= 7;

				pbDest[i] = s1 * 16 + s2;
			}
		}

		/*
		//	[IN] pbSrc - 输入16进制数的起始地址
		//	[IN] nLen - 16进制数的字节数
		// return value: 返回转化出来的字符串
		// remarks : 将16进制数转化为字符串
		*/
		static std::string hex_to_str(const unsigned char *pbSrc, int nLen)
		{
			char ddl, ddh;
			char* pbDest = new char[nLen * 2 + 1]{};
			std::string str;

			for (int i = 0; i < nLen; i++)
			{
				ddh = 48 + pbSrc[i] / 16;
				ddl = 48 + pbSrc[i] % 16;
				if (ddh > 57) ddh = ddh + 7;
				if (ddl > 57) ddl = ddl + 7;
				pbDest[i * 2] = ddh;
				pbDest[i * 2 + 1] = ddl;
			}
			pbDest[nLen * 2] = '\0';

			str.append(pbDest);

			delete[] pbDest;
			return str;
		}
	};
}

// 
// void main()
// {
// 	char *p = "测试短信A01【潍坊科技局】";
// 	std::string url = based::code_decode::url_encode(p);
// 	std::string txt = based::code_decode::url_decode(url.c_str());
// 	system("pause");
// }