# cpp_utils

## 介绍
cpp工具集。

## 目录

```bash
cpp_utils
├─ajson
├─avhttp
│  ├─example
│  ├─include
│  │  └─avhttp
│  │      ├─detail
│  │      └─impl
│  ├─test
│  └─vcprojects
│      └─.vs
│          └─async_http_stream-vc2012
│              └─v14
├─based
├─easyloggingpp
├─encryptor
├─ftplib
├─id_card
│  └─lib
├─img_recognize
├─libxl-3.6.2.0-win
│  ├─bin
│  ├─bin64
│  ├─doc
│  │  └─images
│  ├─examples
│  │  ├─c
│  │  │  ├─mingw
│  │  │  │  └─bin
│  │  │  ├─vs2008
│  │  │  │  ├─custom
│  │  │  │  ├─edit
│  │  │  │  ├─extract
│  │  │  │  ├─format
│  │  │  │  ├─generate
│  │  │  │  ├─invoice
│  │  │  │  └─performance
│  │  │  ├─vs2010
│  │  │  │  ├─custom
│  │  │  │  ├─edit
│  │  │  │  ├─extract
│  │  │  │  ├─format
│  │  │  │  ├─generate
│  │  │  │  ├─invoice
│  │  │  │  └─performance
│  │  │  └─vs2012
│  │  │      ├─custom
│  │  │      ├─edit
│  │  │      ├─extract
│  │  │      ├─format
│  │  │      ├─generate
│  │  │      ├─invoice
│  │  │      └─performance
│  │  ├─c#
│  │  │  ├─vs2008
│  │  │  │  ├─custom
│  │  │  │  │  └─Properties
│  │  │  │  ├─edit
│  │  │  │  │  └─Properties
│  │  │  │  ├─extract
│  │  │  │  │  └─Properties
│  │  │  │  ├─format
│  │  │  │  │  └─Properties
│  │  │  │  ├─generate
│  │  │  │  │  └─Properties
│  │  │  │  ├─invoice
│  │  │  │  │  └─Properties
│  │  │  │  ├─libxl
│  │  │  │  │  └─Properties
│  │  │  │  └─performance
│  │  │  │      └─Properties
│  │  │  ├─vs2010
│  │  │  │  ├─custom
│  │  │  │  │  └─Properties
│  │  │  │  ├─edit
│  │  │  │  │  └─Properties
│  │  │  │  ├─extract
│  │  │  │  │  └─Properties
│  │  │  │  ├─format
│  │  │  │  │  └─Properties
│  │  │  │  ├─generate
│  │  │  │  │  └─Properties
│  │  │  │  ├─invoice
│  │  │  │  │  └─Properties
│  │  │  │  ├─libxl
│  │  │  │  │  └─Properties
│  │  │  │  └─performance
│  │  │  │      └─Properties
│  │  │  └─vs2012
│  │  │      ├─custom
│  │  │      │  └─Properties
│  │  │      ├─edit
│  │  │      │  └─Properties
│  │  │      ├─extract
│  │  │      │  └─Properties
│  │  │      ├─format
│  │  │      │  └─Properties
│  │  │      ├─generate
│  │  │      │  └─Properties
│  │  │      ├─invoice
│  │  │      │  └─Properties
│  │  │      ├─libxl
│  │  │      │  └─Properties
│  │  │      └─performance
│  │  │          └─Properties
│  │  ├─c++
│  │  │  ├─cb
│  │  │  ├─devc
│  │  │  ├─mingw
│  │  │  │  └─bin
│  │  │  ├─qt
│  │  │  ├─vc6
│  │  │  ├─vs2005
│  │  │  ├─vs2008
│  │  │  │  ├─custom
│  │  │  │  ├─edit
│  │  │  │  ├─extract
│  │  │  │  ├─format
│  │  │  │  ├─generate
│  │  │  │  ├─invoice
│  │  │  │  └─performance
│  │  │  ├─vs2010
│  │  │  │  ├─custom
│  │  │  │  ├─edit
│  │  │  │  ├─extract
│  │  │  │  ├─format
│  │  │  │  ├─generate
│  │  │  │  ├─invoice
│  │  │  │  └─performance
│  │  │  └─vs2012
│  │  │      ├─.vs
│  │  │      │  └─libxl
│  │  │      │      └─v14
│  │  │      ├─custom
│  │  │      │  ├─Debug
│  │  │      │  │  └─custom.tlog
│  │  │      │  └─Release
│  │  │      │      └─custom.tlog
│  │  │      ├─Debug
│  │  │      ├─edit
│  │  │      │  ├─Debug
│  │  │      │  │  └─edit.tlog
│  │  │      │  └─Release
│  │  │      │      └─edit.tlog
│  │  │      ├─extract
│  │  │      │  ├─Debug
│  │  │      │  │  └─extract.tlog
│  │  │      │  └─Release
│  │  │      │      └─extract.tlog
│  │  │      ├─format
│  │  │      │  ├─Debug
│  │  │      │  │  └─format.tlog
│  │  │      │  └─Release
│  │  │      │      └─format.tlog
│  │  │      ├─generate
│  │  │      │  ├─Debug
│  │  │      │  │  └─generate.tlog
│  │  │      │  └─Release
│  │  │      │      └─generate.tlog
│  │  │      ├─invoice
│  │  │      │  ├─Debug
│  │  │      │  │  └─invoice.tlog
│  │  │      │  └─Release
│  │  │      │      └─invoice.tlog
│  │  │      ├─performance
│  │  │      │  ├─Debug
│  │  │      │  │  └─performance.tlog
│  │  │      │  └─Release
│  │  │      │      └─performance.tlog
│  │  │      └─Release
│  │  ├─delphi
│  │  ├─fortran
│  │  │  └─vs2008
│  │  │      ├─custom
│  │  │      ├─edit
│  │  │      ├─extract
│  │  │      ├─format
│  │  │      ├─generate
│  │  │      └─invoice
│  │  └─xbase++
│  │      ├─HBLibXL
│  │      └─simple
│  ├─include_c
│  ├─include_cpp
│  ├─lib
│  ├─lib64
│  ├─net
│  └─stdcall
├─media
├─MFC
│  ├─ButtonST
│  │  └─demos
│  │      └─CButton类实例源码
│  │          ├─BtnSTTest
│  │          │  └─res
│  │          ├─CButtonST_demo
│  │          │  ├─.vs
│  │          │  │  └─CButtonST_Demo
│  │          │  │      └─v14
│  │          │  └─res
│  │          └─CButtonST_Source
│  │              └─Source
│  └─ListCtrlEx
│      └─demos
│          └─MFCListCtrlEx
│              ├─.vs
│              │  └─MFCListCtrlEx
│              │      └─v14
│              └─MFCListCtrlEx
│                  └─res
├─mysql
│  └─MySQL Server 5.6
│      ├─include
│      │  └─mysql
│      │      └─psi
│      └─lib
│          ├─debug
│          └─plugin
├─ocilib
│  ├─demo
│  ├─doc
│  │  └─html
│  ├─include
│  ├─lib32
│  ├─lib64
│  ├─proj
│  │  ├─dll
│  │  ├─mingw
│  │  └─test
│  └─src
├─Sm4
├─tools
├─WTL
│  ├─AppWiz
│  │  └─Files
│  │      ├─HTML
│  │      │  └─1033
│  │      ├─Images
│  │      ├─Scripts
│  │      │  └─1033
│  │      └─Templates
│  │          └─1033
│  ├─AppWizCE
│  │  └─Files
│  │      ├─HTML
│  │      │  └─1033
│  │      ├─Images
│  │      ├─Scripts
│  │      │  └─1033
│  │      └─Templates
│  │          └─1033
│  ├─AppWizMobile
│  │  └─Files
│  │      ├─1033
│  │      ├─HTML
│  │      │  └─1033
│  │      ├─Images
│  │      ├─Scripts
│  │      │  └─1033
│  │      └─Templates
│  │          └─1033
│  ├─Include
│  └─Samples
│      ├─Aero
│      │  └─res
│      ├─Alpha
│      │  └─res
│      ├─BmpView
│      │  └─res
│      ├─GuidGen
│      │  └─res
│      ├─ImageView
│      │  └─res
│      ├─MDIDocVw
│      │  └─res
│      ├─MemDlg
│      │  └─res
│      ├─MiniPie
│      │  └─res
│      ├─MTPad
│      │  └─res
│      ├─MTPad7
│      │  └─res
│      ├─SPControls
│      │  └─res
│      ├─TabBrowser
│      │  └─res
│      ├─Wizard97Test
│      │  ├─help
│      │  ├─res
│      │  └─Wizard
│      └─WTLExplorer
│          └─res
└─zlib
    ├─bin
    ├─inc
    └─lib
```



